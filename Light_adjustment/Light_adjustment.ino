/*
	Arduino automatic light adjustment
	
	Uses a photocell to check the amount of light in the room, then adjusts pin 9 with PWM

	(C) Mathias Jensen 2013 ; mjdk.dk
*/

// The pin to adjust PWM
int pwm = 9;

// The pin of the photocell (resistor)
int light = A0;

// Time it takes for adjustment of the light
int timeAdjust = 2000;

// Variable for currentLight, prevents infinite loops (which is quite bad)
int currentLight = 0;
// The delay between light checks
int checkDelay = 5000;

void checkLight() {
	// Measure the current amount of light
	int measure = analogRead(light);
        Serial.println(measure);
	// Use the pre-defined ranges to decide if the light should be adjusted
	if(measure < 400) {
		if(currentLight != 10) {
			adjustLight(pwm, currentLight, 10);
		}
	}
	else if(measure >= 401 && measure <= 500) {
		if(currentLight != 60) {
			adjustLight(pwm, currentLight, 60);
		}
	}
	else if(measure >= 501 && measure <= 700) {
		if(currentLight != 80) {
			adjustLight(pwm, currentLight, 80);
		}
	}
	else if(measure >= 701 && measure <= 850) {
		if(currentLight != 100) {
			adjustLight(pwm, currentLight, 100);
		}
	}
	else if(measure >= 851 && measure <= 950) {
		if(currentLight != 115) {
			adjustLight(pwm, currentLight, 115);
		}
	}
	else if(measure > 950) {
		if(currentLight != 200) {
			adjustLight(pwm, currentLight, 200);
		}
	}

}

void adjustLight(int pin, int from, int to) {

  // Create a variable for containing the range
  int range;

  int isUp;

  // If from is the highest number
  if(from > to) {
    // Range is from subtracted with to
    range = from - to;
    // This means the light will decrease
    isUp = 0;
  } else {
    // Else it is the other way around
    range = to - from;
    // Light will increase
    isUp = 1;
  }
  // Print the range that will be adjusted
  Serial.println("Range to adjust is:");
  Serial.println(range);
  
  if(range != 0) {
  // Change the light gradually
  gradualChange(pwm, to, range, isUp);

  // Set the current light variable to the new value
  currentLight = to;
  
  }
}

void gradualChange(int pin, int to, int range, int isUp) {

	// If light will increase
   if(isUp == 1) {

   		// Loop through the range that the light will be increased by
	   	for(int i = 0; i <= range; i++) {

	   		// Calculate the gradual change
	   		int target = to - range + i;

	   		// Do the actual change of light
	   		analogWrite(pin, target);

	   		// Delay with the time the light should be to adjust divided by the number of gradual changes
	   		delay(timeAdjust/range);

	   	}

   } else {

   		// If the light isn't going to increase it will decrease (no shit)
   		// Loop through the range that the light will be decreased by
		for(int i = 0; i <= range; i++) {
			// Calculate the gradual change
			int target = (to+range) - i;

			// Do the actual change of light
			analogWrite(pin, target);

			// Delay with the time the light should be to adjust divided by the number of gradual changes
	   		delay(timeAdjust/range);
		}
   }
}
